﻿using System;
using System.Text;
using System.IO;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScheduleManagerNS;

namespace ScheduleManagerTests
{
    [TestClass]
    public class ScheduleManagerTests
    {
        public static ScheduleManager scheduleManager;

        [ClassInitialize()]
        public static void ScheduleManagerInitialize(TestContext testContext)
        {
            scheduleManager = new ScheduleManager();
        }

        [TestCleanup()]
        public void ScheduleManagerCleanup()
        {
            scheduleManager.courses.Clear();
        }

        [TestMethod]
        public void ScheduleManagerAddCourseTest()
        {
            Course course1 = new Course("How to write cool apps!",
                                        "Mobile programming",
                                        new DateTime(2012, 5, 10),
                                        new DateTime(2012, 5, 25));

            Course course2 = new Course("Calculus for beginners.",
                                        "Mathematic",
                                        new DateTime(2012, 7, 5),
                                        new DateTime(2012, 8, 15));

            scheduleManager.addCourse(course1);
            scheduleManager.addCourse(course2);

            Assert.AreEqual<int>(2, scheduleManager.courses.Count);
        }

        [TestMethod]
        public void ScheduleManagerAddDuplicateErrorCourseTest()
        {
            Course course1 = new Course("How to write cool apps!",
                                        "Mobile programming",
                                        new DateTime(2012, 5, 10),
                                        new DateTime(2012, 5, 25));

            Course course2 = new Course("Calculus for beginners.",
                                        "Mathematic",
                                        new DateTime(2012, 7, 5),
                                        new DateTime(2012, 8, 15));

            StringWriter consoleOutput = new StringWriter();
            Console.SetError(consoleOutput);

            scheduleManager.addCourse(course1);
            scheduleManager.addCourse(course2);
            scheduleManager.addCourse(course2);

            Assert.AreEqual<int>(2, scheduleManager.courses.Count);
            string expected = string.Format("Such a course already exists.{0}", Environment.NewLine);
            Assert.AreEqual<string>(expected, consoleOutput.ToString());
        }

        [TestMethod]
        public void ScheduleManagerRemoveCourseTest()
        {
            Course course1 = new Course("How to write cool apps!",
                                        "Mobile programming",
                                        new DateTime(2012, 5, 10),
                                        new DateTime(2012, 5, 25));

            Course course2 = new Course("Calculus for beginners.",
                                        "Mathematic",
                                        new DateTime(2012, 7, 5),
                                        new DateTime(2012, 8, 15));

            scheduleManager.addCourse(course1);
            scheduleManager.addCourse(course2);

            scheduleManager.removeCourse(course1);

            Assert.AreEqual<int>(1, scheduleManager.courses.Count);
        }

        [TestMethod]
        public void ScheduleManagerRemoveNotFoundCourseTest()
        {
            Course course1 = new Course("How to write cool apps!",
                                        "Mobile programming",
                                        new DateTime(2012, 5, 10),
                                        new DateTime(2012, 5, 25));

            Course course2 = new Course("Calculus for beginners.",
                                        "Mathematic",
                                        new DateTime(2012, 7, 5),
                                        new DateTime(2012, 8, 15));

            StringWriter consoleOutput = new StringWriter();
            Console.SetError(consoleOutput);

            scheduleManager.addCourse(course1);

            scheduleManager.removeCourse(course2);

            Assert.AreEqual<int>(1, scheduleManager.courses.Count);
            string expected = string.Format("The course 'Calculus for beginners.' was not found.{0}", Environment.NewLine);
            Assert.AreEqual<string>(expected, consoleOutput.ToString());
        }

        [TestMethod]
        public void ScheduleManagerFindCourseByTitleTest()
        {
            Course course1 = new Course("How to write cool apps!",
                                        "Mobile programming",
                                        new DateTime(2012, 5, 10),
                                        new DateTime(2012, 5, 25));

            Course course2 = new Course("Calculus for beginners.",
                                        "Mathematic",
                                        new DateTime(2012, 7, 5),
                                        new DateTime(2012, 8, 15));

            scheduleManager.addCourse(course1);
            scheduleManager.addCourse(course2);

            Course found_course = scheduleManager.findCourseByTitle("Calculus");

            Assert.AreEqual<Course>(found_course, course2);
        }

        [TestMethod]
        public void ScheduleManagerFindCourseByTitleNotFoundTest()
        {
            Course course1 = new Course("How to write cool apps!",
                                        "Mobile programming",
                                        new DateTime(2012, 5, 10),
                                        new DateTime(2012, 5, 25));

            Course course2 = new Course("Calculus for beginners.",
                                        "Mathematic",
                                        new DateTime(2012, 7, 5),
                                        new DateTime(2012, 8, 15));

            scheduleManager.addCourse(course1);
            scheduleManager.addCourse(course2);

            Course found_course = scheduleManager.findCourseByTitle("Super applications");

            Assert.AreEqual<Course>(found_course, (Course)null);
        }

        [TestMethod]
        public void ScheduleManagerChangeStatusCourseTest()
        {
            Course course1 = new Course("How to write cool apps!",
                                        "Mobile programming",
                                        new DateTime(2012, 5, 10),
                                        new DateTime(2012, 5, 25));

            Course course2 = new Course("Calculus for beginners.",
                                        "Mathematic",
                                        new DateTime(2012, 7, 5),
                                        new DateTime(2012, 8, 15));

            StringWriter consoleOutput = new StringWriter();
            Console.SetError(consoleOutput);

            scheduleManager.addCourse(course1);
            scheduleManager.addCourse(course2);

            Administrator admin = new Administrator("Max", "Weber");
            int i = scheduleManager.changeStatusOfCourse(CourseStatus.Completed, course1, admin);

            Assert.AreEqual<CourseStatus>(CourseStatus.Completed, scheduleManager.courses[0].status);
        }

        [TestMethod]
        public void ScheduleManagerChangeStatusCourseNotFoundTest()
        {
            Course course1 = new Course("How to write cool apps!",
                                        "Mobile programming",
                                        new DateTime(2012, 5, 10),
                                        new DateTime(2012, 5, 25));

            Course course2 = new Course("Calculus for beginners.",
                                        "Mathematic",
                                        new DateTime(2012, 7, 5),
                                        new DateTime(2012, 8, 15));

            Course course3 = new Course("Let's create web-page in 5 minutes.",
                                        "Web-programming",
                                        new DateTime(2012, 2, 10),
                                        new DateTime(2012, 2, 20));

            StringWriter consoleOutput = new StringWriter();
            Console.SetError(consoleOutput);

            scheduleManager.addCourse(course1);
            scheduleManager.addCourse(course2);

            Administrator admin = new Administrator("Max", "Weber");
            scheduleManager.changeStatusOfCourse(CourseStatus.Completed, course3, admin);

            string expected = string.Format("The course 'Let's create web-page in 5 minutes.' was not found.{0}", Environment.NewLine);
            Assert.AreEqual<string>(expected, consoleOutput.ToString());
        }
    }
}
