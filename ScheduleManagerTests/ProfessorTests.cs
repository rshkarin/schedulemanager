﻿using System;
using System.Text;
using System.Collections.Generic;
using ScheduleManagerNS;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ScheduleManagerTests
{
    [TestClass]
    public class ProfessorTests
    {
        [TestMethod]
        public void ProfessorInitializationTest()
        {
            string firstName = "Jeremmy";
            string lastName = "Fessler";
            DateTime birthDay = new DateTime(1965,7,25);
            string degree = "PhD";
            
            Professor professor = new Professor(firstName, lastName, degree, birthDay);

            Assert.IsTrue(professor.isInitialized);
            StringAssert.Equals(firstName, professor.firstName);
            StringAssert.Equals(lastName, professor.lastName);
            StringAssert.Equals(degree, professor.degree);
            Assert.AreEqual<DateTime>(birthDay, professor.birthDay.Value);
        }

        [TestMethod]
        public void ProfessorFailedInitializationTest()
        {
            string firstName = "Jeremmy";
            string lastName = "";
            string degree = "PhD";

            Professor professor = new Professor(firstName, lastName, degree);

            Assert.IsFalse(professor.isInitialized);
        }
    }
}
