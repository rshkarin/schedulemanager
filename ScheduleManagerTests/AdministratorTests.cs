﻿using System;
using System.Text;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScheduleManagerNS;

namespace ScheduleManagerTests
{
    [TestClass]
    public class AdministratorTests
    {
        [TestMethod]
        public void AdministratorInitializationTest()
        {
            string firstName = "Max";
            string lastName = "Weller";
            DateTime birthDay = new DateTime(1960, 3, 15);

            Administrator admin = new Administrator(firstName, lastName, birthDay);

            Assert.IsTrue(admin.isInitialized);
            StringAssert.Equals(firstName, admin.firstName);
            StringAssert.Equals(lastName, admin.lastName);
            Assert.AreEqual<DateTime>(birthDay, admin.birthDay.Value);
        }

        [TestMethod]
        public void AdministratorFailedInitializationTest()
        {
            string firstName = "Max";
            string lastName = "";
            DateTime birthDay = new DateTime(1960, 3, 15);

            Administrator admin = new Administrator(firstName, lastName, birthDay);

            Assert.IsFalse(admin.isInitialized);
            StringAssert.Equals(firstName, admin.firstName);
            Assert.AreEqual<DateTime>(birthDay, admin.birthDay.Value);
        }
    }
}
