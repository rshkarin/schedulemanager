﻿using System;
using ScheduleManagerNS;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ScheduleManagerTests
{
    [TestClass]
    public class StudentTests
    {
        [TestMethod]
        public void StudentInitializationTest()
        {
            string firstName = "John";
            string lastName = "Smith";
            DateTime birthDay = new DateTime(1990, 11, 2);
            int studentId = 20125;
            string groupNumber = "8BM22";
            string universityName = "KIT";

            Student student = new Student(firstName, lastName, studentId, birthDay, universityName, groupNumber);

            Assert.IsTrue(student.isInitialized);
            StringAssert.Equals(firstName, student.firstName);
            StringAssert.Equals(lastName, student.lastName);
            StringAssert.Equals(studentId, student.personalId);
            Assert.AreEqual<DateTime>(birthDay, student.birthDay.Value);
            StringAssert.Equals(groupNumber, student.groupNumber);
            StringAssert.Equals(universityName, student.universityName);
        }

        [TestMethod]
        public void StudentFailedInitializationTest()
        {
            string firstName = "John";
            string lastName = "";
            DateTime birthDay = new DateTime(1990, 11, 2);
            int studentId = 0;
            string groupNumber = "8BM22";
            string universityName = "KIT";

            Student student = new Student(firstName, lastName, studentId, birthDay, universityName, groupNumber);

            Assert.IsFalse(student.isInitialized);
            StringAssert.Equals(firstName, student.firstName);
            StringAssert.Equals(lastName, student.lastName);
            StringAssert.Equals(studentId, student.personalId);
            Assert.AreEqual<DateTime>(birthDay, student.birthDay.Value);
            StringAssert.Equals(groupNumber, student.groupNumber);
            StringAssert.Equals(universityName, student.universityName);
        }
    }
}
