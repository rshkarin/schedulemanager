﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ScheduleManagerNS;

namespace ScheduleManagerTests
{
    [TestClass]
    public class CourseTests
    {
        public static string title;
        public static string field;
        public static DateTime startDate;
        public static DateTime finishDate;
        public static CourseStatus status;
        public static int ageRestriction;
        public static string adminType;

        public static Course course { set; get; }
        public static Course course_no_restrictions { set; get; }

        [ClassInitialize()]
        public static void CourseInitialize(TestContext testContext) 
        {
            CourseTests.title = "Digital Signal Processing for beginners.";
            CourseTests.field = "Digital Signal Processing";
            CourseTests.startDate = new DateTime(2010, 10, 10);
            CourseTests.finishDate = new DateTime(2010, 10, 20);
            CourseTests.ageRestriction = 16;
            CourseTests.adminType = typeof(Administrator).ToString();
            CourseTests.status = CourseStatus.Waiting;

            course = new Course(CourseTests.title, 
                CourseTests.field, 
                CourseTests.startDate, 
                CourseTests.finishDate,
                CourseTests.adminType, 
                CourseTests.ageRestriction);

            course_no_restrictions = new Course(CourseTests.title,
                CourseTests.field,
                CourseTests.startDate,
                CourseTests.finishDate,
                CourseTests.adminType,
                0);
        }

        [TestCleanup()]
        public void CourseCleanup()
        {
            course = null;
            course = new Course(CourseTests.title,
                CourseTests.field,
                CourseTests.startDate,
                CourseTests.finishDate,
                CourseTests.adminType,
                CourseTests.ageRestriction);

            course_no_restrictions = null;
            course_no_restrictions = new Course(CourseTests.title,
                CourseTests.field,
                CourseTests.startDate,
                CourseTests.finishDate,
                CourseTests.adminType,
                0);
        }

        [TestMethod]
        public void CourseInitializationTest()
        {
            StringAssert.Equals(CourseTests.title, course.title);
            StringAssert.Equals(CourseTests.field, course.field);
            Assert.AreEqual<DateTime>(CourseTests.startDate, course.startDate.Value);
            Assert.AreEqual<DateTime>(CourseTests.finishDate, course.finishDate.Value);
            Assert.AreEqual<int>(CourseTests.ageRestriction, course.ageRestriction);
            StringAssert.Equals(CourseTests.adminType, course.adminType);
        }

        [TestMethod]
        public void CourseAddKeywordTest()
        {
            string kw1 = "DSP";
            string kw2 = "FFT";
            string kw3 = "Interpolation";

            course.addKeyWord(kw1);
            course.addKeyWord(kw2);
            course.addKeyWord(kw3);

            Assert.AreEqual<int>(3, course.keyWords.Count);
        }

        [TestMethod]
        public void CourseRemoveKeywordTest()
        {
            string kw1 = "DSP";
            string kw2 = "FFT";
            string kw3 = "Interpolation";

            course.addKeyWord(kw1);
            course.addKeyWord(kw2);
            course.addKeyWord(kw3);

            course.removeKeyword(kw2);
            course.removeKeyword(kw1);

            Assert.AreEqual<int>(1, course.keyWords.Count);
        }

        [TestMethod]
        public void CourseAddKeywordExceedTest()
        {
            string kw1 = "DSP";
            string kw2 = "FFT";
            string kw3 = "Interpolation";
            string kw4 = "Sinc";
            string kw5 = "LWFilter";
            string kw6 = "HPFilter";

            StringWriter consoleOutput = new StringWriter();
            Console.SetError(consoleOutput);

            course.addKeyWord(kw1);
            course.addKeyWord(kw2);
            course.addKeyWord(kw3);
            course.addKeyWord(kw4);
            course.addKeyWord(kw5);
            course.addKeyWord(kw6);

            Assert.AreEqual<int>(5, course.keyWords.Count);
            string expected = string.Format("The maximum number of keywords has been exceeded.{0}", Environment.NewLine);
            Assert.AreEqual<string>(expected, consoleOutput.ToString());
        }

        [TestMethod]
        public void CourseRemoveKeywordNotFoundTest()
        {
            string kw1 = "DSP";
            string kw2 = "FFT";
            string kw3 = "Interpolation";
            string kw4 = "Sinc";
            string kw5 = "LWFilter";

            StringWriter consoleOutput = new StringWriter();
            Console.SetError(consoleOutput);

            course.addKeyWord(kw1);
            course.addKeyWord(kw2);
            course.addKeyWord(kw3);
            course.addKeyWord(kw4);

            course.removeKeyword(kw5);

            Assert.AreEqual<int>(4, course.keyWords.Count);
            string expected = string.Format("The keyword LWFilter was not found.{0}", Environment.NewLine);
            Assert.AreEqual<string>(expected, consoleOutput.ToString());
        }

        [TestMethod]
        public void CourseAddProfessorTest()
        {
            Professor professor = new Professor("John", "Smith", "PhD");

            course.addProfessor(professor);

            Assert.AreEqual<int>(1, course.professors.Count);
        }

        [TestMethod]
        public void CourseAddProfessorNotInitializedTest()
        {
            Professor professor = new Professor("John", "", "PhD");

            StringWriter consoleOutput = new StringWriter();
            Console.SetError(consoleOutput);

            course.addProfessor(professor);

            Assert.AreEqual<int>(0, course.professors.Count);
            string expected = string.Format("Important fields haven't been defined.{0}", Environment.NewLine);
            Assert.AreEqual<string>(expected, consoleOutput.ToString());
        }

        [TestMethod]
        public void CourseRemoveProfessorWithPhDTest()
        {
            Professor professor = new Professor("John", "Smith", "PhD");

            course.addProfessor(professor);
            course.removeProfessor(professor);

            Assert.AreEqual<int>(0, course.professors.Count);
        }

        [TestMethod]
        public void CourseRemoveProfessorWithPhDNotFoundTest()
        {
            Professor professor = new Professor("John", "Smith", "PhD");
            Professor professor_another = new Professor("Jeremy", "Fessler", "PhD");

            StringWriter consoleOutput = new StringWriter();
            Console.SetError(consoleOutput);

            course.addProfessor(professor);
            course.removeProfessor(professor_another);

            Assert.AreEqual<int>(1, course.professors.Count);
            string expected = string.Format("The professor PhD, Jeremy Fessler was not found.{0}", Environment.NewLine);
            Assert.AreEqual<string>(expected, consoleOutput.ToString());
        }

        [TestMethod]
        public void CourseRemoveProfessorWithNotFoundTest()
        {
            Professor professor = new Professor("John", "Smith", "PhD");
            Professor professor_another = new Professor("Adam", "Jefferson", "");

            StringWriter consoleOutput = new StringWriter();
            Console.SetError(consoleOutput);

            course.addProfessor(professor);
            course.removeProfessor(professor_another);

            Assert.AreEqual<int>(1, course.professors.Count);
            string expected = string.Format("The professor Adam Jefferson was not found.{0}", Environment.NewLine);
            Assert.AreEqual<string>(expected, consoleOutput.ToString());
        }

        [TestMethod]
        public void CourseAddStudentTest()
        {
            Student student1 = new Student("Mike", "Lesterman", 12345, new DateTime(1985,3,15));
            Student student2 = new Student("Sandra", "Koppmann", 45213, new DateTime(1982, 5, 12));

            course.addStudent(student1);
            course.addStudent(student2);

            Assert.AreEqual<int>(2, course.students.Count);
        }

        [TestMethod]
        public void CourseAddStudentWithoutAgeRestrictionTest()
        {
            Student student1 = new Student("Mike", "Lesterman", 12345, new DateTime(2008, 3, 15));
            Student student2 = new Student("Sandra", "Koppmann", 45213, new DateTime(2009, 5, 12));

            course_no_restrictions.addStudent(student1);
            course_no_restrictions.addStudent(student2);

            Assert.AreEqual<int>(2, course_no_restrictions.students.Count);
        }

        [TestMethod]
        public void CourseAddStudentNotInitializedTest()
        {
            Student student1 = new Student("Mike", "Lesterman", 12345, new DateTime(1985, 3, 15));
            Student student2 = new Student("Sandra", "", -1, new DateTime(1982, 5, 12));

            StringWriter consoleOutput = new StringWriter();
            Console.SetError(consoleOutput);

            course.addStudent(student1);
            course.addStudent(student2);

            Assert.AreEqual<int>(1, course.students.Count);
            string expected = string.Format("Important fields haven't been defined.{0}", Environment.NewLine);
            Assert.AreEqual<string>(expected, consoleOutput.ToString());
        }

        [TestMethod]
        public void CourseAddStudentBirthdayIsNullTest()
        {
            Student student1 = new Student("Mike", "Lesterman", 12345, new DateTime(1985, 3, 15));
            Student student2 = new Student("Sandra", "Koppmann", 4623);

            StringWriter consoleOutput = new StringWriter();
            Console.SetError(consoleOutput);

            course.addStudent(student1);
            course.addStudent(student2);

            Assert.AreEqual<int>(1, course.students.Count);
            string expected = string.Format("You should set the date of birthday, before trying to subscribe.{0}", Environment.NewLine);
            Assert.AreEqual<string>(expected, consoleOutput.ToString());
        }

        [TestMethod]
        public void CourseAddStudentBirthdaySmallAgeTest()
        {
            Student student1 = new Student("Mike", "Lesterman", 12345, new DateTime(1985, 3, 15));
            Student student2 = new Student("Sandra", "Koppmann", 4623, new DateTime(2008, 3, 15));

            StringWriter consoleOutput = new StringWriter();
            Console.SetError(consoleOutput);

            course.addStudent(student1);
            course.addStudent(student2);

            Assert.AreEqual<int>(1, course.students.Count);
            string expected = string.Format("Your age doesn't allow you to attend.{0}", Environment.NewLine);
            Assert.AreEqual<string>(expected, consoleOutput.ToString());
        }

        [TestMethod]
        public void CourseRemoveStudentTest()
        {
            Student student1 = new Student("Mike", "Lesterman", 12345, new DateTime(1985, 3, 15));
            Student student2 = new Student("Sandra", "Koppmann", 4623, new DateTime(1970, 3, 15));

            course.addStudent(student1);
            course.addStudent(student2);

            course.removeStudent(student2);

            Assert.AreEqual<int>(1, course.students.Count);
        }

        [TestMethod]
        public void CourseRemoveStudentNotFoundTest()
        {
            Student student1 = new Student("Mike", "Lesterman", 12345, new DateTime(1985, 3, 15));
            Student student2 = new Student("Sandra", "Koppmann", 4623, new DateTime(1970, 3, 15));

            StringWriter consoleOutput = new StringWriter();
            Console.SetError(consoleOutput);

            course.addStudent(student1);

            course.removeStudent(student2);

            Assert.AreEqual<int>(1, course.students.Count);
            string expected = string.Format("The student Sandra Koppmann (4623) was not found.{0}", Environment.NewLine);
            Assert.AreEqual<string>(expected, consoleOutput.ToString());
        }

        [TestMethod]
        public void CourseChangeStatusTest()
        {
            Administrator admin = new Administrator("Max", "Weber");

            Assert.AreEqual<CourseStatus>(CourseStatus.None, course.status);

            course.changeStatus(CourseStatus.Completed, admin);

            Assert.AreEqual<CourseStatus>(CourseStatus.Completed, course.status);
        }

        [TestMethod]
        public void CourseChangeStatusNoPrivilegesTest()
        {
            Student student = new Student("Roland", "Cooper", 12052);

            StringWriter consoleOutput = new StringWriter();
            Console.SetError(consoleOutput);

            course.changeStatus(CourseStatus.Completed, student);

            Assert.AreEqual<CourseStatus>(CourseStatus.None, course.status);

            string expected = string.Format("You don't have a administrator privileges.{0}", Environment.NewLine);
            Assert.AreEqual<string>(expected, consoleOutput.ToString());
        }
    }
}
