﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagerNS
{
    public class Professor : User
    {
        public string degree { get; private set; }

        public Professor() 
            : base()
        {
            this.degree = "";
        }

        public Professor(string firstName, string lastName) 
            : this()
        {
            this.firstName = firstName;
            this.lastName = lastName;

            if (this.firstName != "" && this.lastName != "")
            {
                this.isInitialized = true;
            }
            else
            {
                Console.Error.WriteLine("Some of fields have been initialized in an incorrect way.");
            }
        }

        public Professor(string firstName, string lastName, string degree)
            : this(firstName, lastName)
        {
            this.degree = degree;
        }

        public Professor(string firstName, string lastName, string degree, DateTime birthDay)
            : this(firstName, lastName, degree)
        {
            this.birthDay = birthDay;
        }
    }
}
