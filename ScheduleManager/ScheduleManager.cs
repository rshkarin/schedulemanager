﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagerNS
{
    public class ScheduleManager
    {
        public List<Course> courses { private set; get; }

        public string adminType { private set; get; }

        public ScheduleManager()
        {
            this.courses = new List<Course>();
            this.adminType = typeof(Administrator).ToString();
        }

        public void addCourse(Course course)
        {
            if (!this.courses.Contains(course))
            {
                course.status = CourseStatus.Waiting;
                course.adminType = this.adminType;
                this.courses.Add(course);
            }
            else {
                Console.Error.WriteLine("Such a course already exists.");
            }
        }

        public void removeCourse(Course course)
        {
            if (!this.courses.Remove(course))
            {
                Console.Error.WriteLine("The course '{0}' was not found.", course.title);
            }
        }

        public Course findCourseByTitle(string title)
        {
            Course course = this.courses.Find(
                delegate(Course c)
                {
                    bool contains = c.title.IndexOf(title, StringComparison.OrdinalIgnoreCase) >= 0;
                    return contains;
                });

            return course;
        }

        public int changeStatusOfCourse(CourseStatus newStatus, Course course, User user)
        {
            var i = this.courses.FindIndex(c => c.title == course.title);
            if (i >= 0)
            {
                this.courses[i].changeStatus(newStatus, user);
            }
            else {
                Console.Error.WriteLine("The course '{0}' was not found.", course.title);
            }

            return (int)i;
        }
    }
}
