﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagerNS
{
    public class Student : User
    {
        public string universityName { set; get; }
        public string groupNumber { set; get; }
        public int personalId { set; get; }

        public Student() 
            : base()
        {
            this.universityName = "";
            this.groupNumber = "";
            this.personalId = -1;
        }

        public Student(string firstName, string lastName, int personalId)
            : this()
        {
            this.firstName = firstName;
            this.lastName = lastName;
            this.personalId = personalId;

            if (this.firstName != "" && this.lastName != "" && this.personalId > 0)
            {
                this.isInitialized = true;
            }
            else {
                Console.Error.WriteLine("Some of fields have been initialized in an incorrect way.");
            }
        }

        public Student(string firstName, string lastName, int personalId, DateTime birthDay)
            : this(firstName, lastName, personalId)
        {
            this.birthDay = birthDay;
        }

        public Student(string firstName, string lastName, int personalId, DateTime birthDay, string universityName)
            : this(firstName, lastName, personalId, birthDay)
        {
            this.universityName = universityName;
        }

        public Student(string firstName, string lastName, int personalId, DateTime birthDay, string universityName, string groupNumber)
            : this(firstName, lastName, personalId, birthDay, universityName)
        {
            this.universityName = universityName;
        }
    }
}
