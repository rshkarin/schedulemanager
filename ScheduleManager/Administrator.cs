﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagerNS
{
    public class Administrator : User
    {
        public Administrator(string firstName, string lastName)
            : base()
        {
            this.firstName = firstName;
            this.lastName = lastName;

            if (this.firstName != "" && this.lastName != "")
            {
                this.isInitialized = true;
            }
            else
            {
                Console.Error.WriteLine("Some of fields have been initialized in an incorrect way.");
            }
        }

        public Administrator(string firstName, string lastName, DateTime birthDay)
            : this(firstName, lastName)
        {
            this.birthDay = birthDay;
        }
    }
}
