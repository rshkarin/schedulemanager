﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagerNS
{
    public abstract class User
    {
        public string firstName { get; protected set; }
        public string lastName { get; protected set; }
        public Nullable<DateTime> birthDay { get; protected set; }
        public bool isInitialized { get; protected set; }

        protected User() 
        {
            this.firstName = "";
            this.lastName = "";
            this.birthDay = null;
            this.isInitialized = false;
        }
    }
}
