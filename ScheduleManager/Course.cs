﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ScheduleManagerNS
{
    public enum CourseStatus { None, Waiting, Cancelled, Completed, Moved }

    public class Course
    {
        public string title { private set; get; }
        public string field { private set; get; }

        public Nullable<DateTime> startDate { private set; get; }
        public Nullable<DateTime> finishDate { private set; get; }

        public int ageRestriction { private set; get; }

        public CourseStatus status { set; get; }

        public List<Professor> professors { private set; get; }
        public List<Student> students { private set; get; }

        public List<string> keyWords { private set; get; }

        public string adminType { set; get; }
        public int maxNumKeyWords { private set; get; }

        public Course()
        {
            this.title = "";
            this.field = "";
            this.startDate = null;
            this.finishDate = null;
            this.ageRestriction = -1;
            this.status = CourseStatus.None;
            this.professors = new List<Professor>();
            this.students = new List<Student>();
            this.keyWords = new List<string>();
            this.adminType = "";
            this.maxNumKeyWords = 5;
        }

        public Course(string title, string field, DateTime startDate, DateTime finishDate)
            : this()
        {
            this.title = title;
            this.field = field;
            this.startDate = startDate;
            this.finishDate = finishDate;
        }

        public Course(string title, string field, DateTime startDate, DateTime finishDate, string adminType)
            : this(title, field, startDate, finishDate)
        {
            this.adminType = adminType;
        }

        public Course(string title, string field, DateTime startDate, DateTime finishDate, string adminType, int ageRestriction)
            : this(title, field, startDate, finishDate, adminType)
        {
            this.ageRestriction = ageRestriction;
        }

        public bool Equals(Course c)
        {
            if ((object)c == null)
            {
                return false;
            }

            return (c.title == this.title) &&
                   (c.field == this.field) &&
                   (c.startDate == this.startDate) &&
                   (c.finishDate == this.finishDate) &&
                   (c.status == this.status) &&
                   (c.adminType == this.adminType) &&
                   (c.ageRestriction == this.ageRestriction);
        }

        public void addKeyWord(string word)
        {
            if (this.keyWords.Count >= this.maxNumKeyWords)
            {
                Console.Error.WriteLine("The maximum number of keywords has been exceeded.");
            }
            else {
                this.keyWords.Add(word);
            }
        }

        public void removeKeyword(string word) 
        {
            if (!this.keyWords.Remove(word)) 
            {
                Console.Error.WriteLine("The keyword {0} was not found.", word);
            }
        }

        public void addProfessor(Professor professor)
        {
            if (professor.isInitialized)
            {
                this.professors.Add(professor);
            }
            else {
                Console.Error.WriteLine("Important fields haven't been defined.");
            }
        }

        public void removeProfessor(Professor professor)
        {
            if (!this.professors.Remove(professor))
            {
                if (professor.degree != "")
                {
                    Console.Error.WriteLine("The professor {0}, {1} {2} was not found.", professor.degree, professor.firstName, professor.lastName);
                }
                else
                {
                    Console.Error.WriteLine("The professor {0} {1} was not found.", professor.firstName, professor.lastName);
                }
            }
        }

        public void addStudent(Student student)
        {
            if (!student.isInitialized)
            {
                Console.Error.WriteLine("Important fields haven't been defined.");
            }
            else {
                if (this.ageRestriction != 0)
                {
                    if (student.birthDay != null)
                    {
                        DateTime today = DateTime.Today;
                        int ageOffset = (today.Year - student.birthDay.Value.Year);

                        if (ageOffset >= this.ageRestriction)
                        {
                            this.students.Add(student);
                        }
                        else
                        {
                            Console.Error.WriteLine("Your age doesn't allow you to attend.");
                        }
                    }
                    else
                    {
                        Console.Error.WriteLine("You should set the date of birthday, before trying to subscribe.");
                    }
                }
                else
                {
                    this.students.Add(student);
                }
            }
        }

        public void removeStudent(Student student) 
        {
            if (!this.students.Remove(student))
            {
                Console.Error.WriteLine("The student {0} {1} ({2}) was not found.", student.firstName, student.lastName, student.personalId);
            }
        }

        public void changeStatus(CourseStatus newStatus, User user)
        {
            if (this.adminType != user.GetType().ToString())
            {
                Console.Error.WriteLine("You don't have a administrator privileges.");
            }
            else {
                this.status = newStatus;
            }
        }
    }
}
